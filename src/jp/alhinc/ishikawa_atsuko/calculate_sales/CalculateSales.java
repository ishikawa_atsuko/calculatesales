package jp.alhinc.ishikawa_atsuko.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class CalculateSales {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			//naemesは（支店コード、支店名）：blanch.list
			HashMap<String, String> names = new HashMap<>();

			//amountsは（売上ファイルの支店コード、支店合計金額（0円））
			HashMap<String, Long> amounts = new HashMap<>();

			BufferedReader br = null;
			try {

				//branch.lstを取り込み
				File branchLst = new File(args[0], "branch.lst");
				if(!branchLst.exists()) {
					System.out.println("支店定義ファイルが存在しません。");
					return;
				}

				br = new BufferedReader(new FileReader(branchLst));

				//branch.lstを一行ずつ取り込み
				String line;
				while ((line = br.readLine()) != null) {
					//読み込んだファイルを[,]で分ける
					String[] name = line.split(",");

					if (name.length != 2 || !name[0].matches("\\d{3}")){
						System.out.println(name[0]);
						System.out.println("支店定義ファイルのフォーマットが不正です。");
						return;
					}

					//namesに支店コードと支店名をput
					names.put(name[0], name[1]);

					//amountsに支店コードと支店合計額（0円）をput
					amounts.put(name[0], 0l);

				}
			} finally {
				if (br != null) {
					br.close();
				}
			}

			br = null;
			try {
				//rcdFilenamesは「売上ファイル名」
				ArrayList<String> rcdFilenames = new ArrayList<>();

				File directory = new File(args[0]);

				//売上ファイル一覧を出す
				File files[] = directory.listFiles();

				//売上ファイルの名前を取得
				for (File file : files) {
					String filename = file.getName();

					//8桁の数字の名前　かつ　"rcd"ファイル名を検索
					if (!filename.matches("^\\d{8}.rcd$")) {
						continue;
					}

					//拡張子が付いたファイル名をal5にadd
					rcdFilenames.add(filename);
				}

				//売上ファイルが連番になってない時のエラー処理　（3.エラー処理　2-1）
				int min = Integer.parseInt(rcdFilenames.get(0).substring(0, 8));
				int max = Integer.parseInt(rcdFilenames.get(rcdFilenames.size() -1).substring(0, 8));
				if (min + rcdFilenames.size() - 1 != max) {
					System.out.println("売上ファイルが連番になっていません。");
					return;
				}

				for (String rcdFilename : rcdFilenames) {
					//検索したファイルを1行ずつ読み込み、支店コードと売上金額に分ける
					File rcdFile = new File(args[0], rcdFilename);
					br = new BufferedReader(new FileReader(rcdFile));

					//1行目（支店コード）をString型のまま読み込む
					String code = br.readLine();

					//2行目（売上金額）を読み込み、String型からLong型にキャスト
					Long amount = Long.parseLong(br.readLine());

					//3行目を読み込む
					String line3 = br.readLine();

					//3行目に何か入力があるときのエラー処理　（3.エラー処理　2-4）
					if (line3 != null){
						System.out.println(names.get(rcdFilename) + "のフォーマットが不正です");
						return;
					}

					//売上ファイルの支店コードが支店定義ファイルの支店コードになければエラー処理　（3.エラー処理　2-3）
					if (!names.containsKey(code)) {
						System.out.println(rcdFilename + "の支店コードが不正です");
						return;
					}

					//map2から支店合計額を持ってきて、それと売上金額を合算したものをmap2へput
					Long total = amounts.get(code);
					total += amount;

					//map2の合計金額が10桁超えたらエラー処理　（3.エラー処理　2-2）
					if (total.toString().length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}

					amounts.put(code, total);
				}
			} finally {
				if (br != null) {
					br.close();
				}
			}

			//支店別集計ファイルを作成する
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(new File(args[0], "branch.out"))));
			for (Entry<String, String> entry : names.entrySet()) {
				pw.write(entry.getKey() + "," + entry.getValue() + "," + amounts.get(entry.getKey()) + "\r\n");
			}
			pw.close();
			//エラーが出たとき　（3.エラー処理　3-1　その②）

		} catch(Exception e) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}
	}
}
