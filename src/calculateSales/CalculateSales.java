package calculateSales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculateSales {
	public static void main(String[] args){

		//		 System.out.println("ここにあるファイルを開きます = >" + args[0]);
		//map1は（支店コード、支店名）
		HashMap<String,String> map1 = new HashMap<String,String>();
		//map2は（支店コード、売上合計金額）
		HashMap<String,Long> map2 = new HashMap<String,Long>();
		//alは「支店ファイル名（拡張子なし）」
		ArrayList<Integer> al = new ArrayList<Integer>();
		ArrayList<String> al2 = new ArrayList<String>();
		ArrayList<String> al3 = new ArrayList<String>();

		BufferedReader br = null;
		try {

			//branch.lstを取り込み
			File file = new File(args[0], "branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//branch.lstを一行ずつ取り込み
			String line;
			while ((line = br.readLine()) != null) {
				String str = line;
				Pattern p2 = Pattern.compile(".*[,].*");
				Matcher m2 = p2.matcher(str);

				if( m2.find() == false){
					System.out.println("支店定義ファイルのフォーマットが不正です。");
					System.exit(0);

				} else {

					String[] name = str.split(",");

					Pattern p = Pattern.compile("^\\d{3}$");
					Pattern p3 = Pattern.compile(".*\\r\\n.*");
					Pattern p4 = Pattern.compile("^[0-9]*$");
					Matcher m = p.matcher(name[0]);
					Matcher m3 = p3.matcher(name[1]);
					Matcher m4 = p4.matcher(name[1]);

					if ( m.find() == false || m3.find() == true || m4.find() == true){
						System.out.println("支店定義ファイルのフォーマットが不正です。");
						br.close();
						System.exit(0);

					} else {
						//map1に支店コードと支店名をput
						map1.put(name[0],name[1]);

						//map2に支店コードと支店合計額（0円）をput
						map2.put(name[0],0l);

						//al2に支店コードを配列に入れる
						al2.add(name[0]);
					}
				}
			}

			//ファイルがなかった時のエラー処理
		} catch (FileNotFoundException e ) {
			System.out.println("支店定義ファイルが存在しません。");
			System.exit(0);

			//エラーが出たとき
		} catch (IOException e) {
			System.out.println("エラーが発生しました。");

		} finally {

			if (br != null) {
				try {
					br.close();

				} catch (IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}

		//		 for (Entry<String, String> entry : map.entrySet()) {
		//			 System.out.println(entry.getKey() + ":" + entry.getValue());   //支店コードと支店名を別々で表示
		//		 }


		BufferedReader bu = null;
		try {
			File filee = new File(args[0]);

			//売上ファイル一覧を出す
			File files[] = filee.listFiles();


			for (int i = 0; i < files.length; i++){
				String st = files[i].getName();

				//8桁の数字の名前　かつ　"rcd"ファイル名を検索
				if (st.endsWith("rcd") && st.matches(".*[0-9]{8}.*")){
					File fileee = new File(args[0],st);
					FileReader fre = new FileReader(fileee);
					bu = new BufferedReader(fre);
					String lines = bu.readLine();
					Long lines2 = Long.parseLong(bu.readLine());


					String renban = st.substring(0,st.lastIndexOf('.'));
					int renban2 = Integer.parseInt(renban);

					//alにファイル名（拡張子なし）を配列に入れる
					al.add(renban2);

					//map2に支店コードと売上金額をput(支店合計額と売上金額が足される)
					map2.put(lines,lines2);

					//al3に支店コードを配列に入れる
					al3.add(lines);
				}
			}
			for ( int in = 0; in < al.size(); in++ ){
				if (!(( al.size()-1) == (al.get(al.size()-1)-al.get(0)))){
					System.out.println("売上ファイルが連番になっていません。");
					bu.close();
					System.exit(0);
				}

			}

			for ( long integ : map2.values()){
				int keta = String.valueOf(integ).length();
				if ( keta > 10) {
					System.out.println("合計金額が10桁を超えました。");
					bu.close();
					System.exit(0);
				}
			}
			for ( int in2 = 0; in2 < al2.size(); in2++){
				if ( ! al2.get(in2).equals(al3.get(in2))){
					System.out.println( map1.get(al2.get(in2)) + "の支店コードは不正です。");
					bu.close();
					System.exit(0);

				}
			}
			PrintWriter pw = new PrintWriter ( new BufferedWriter ( new FileWriter ("C:\\Users\\admin\\Desktop\\売り上げ集計課題\\branch.out")));
			for ( Entry <String, String> entry : map1.entrySet()) {
				pw.write ( entry.getKey() + "," + entry.getValue() + "," + map2.get (entry.getKey()) + "\r\n" );
				//			 System.out.println(entry.getKey() + "," + entry.getValue() + "," + map2.get(entry.getKey()));
			}
			pw.close();

		}
		catch (IOException e){
			System.out.println("エラーが発生しました。");
		} finally {
			if (bu != null) {
				try {
					bu.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}
}
